import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import Carousel from 'react-bootstrap/Carousel';


import 'font-awesome/css/font-awesome.min.css';

import './App.css';
import image1 from './assets/images/anh_mon_1.jpg'
import image2 from './assets/images/anh_mon_2.jpg'
import image3 from './assets/images/anh_mon_3.jpg'
import image4 from './assets/images/anh_mon_4.jpg'
import image5 from './assets/images/anh_mon_5.jpg'
import image6 from './assets/images/anh_mon_6.jpg'
import image7 from './assets/images/anh_mon_7.jpg'



import HeaderComponent from './Components/HeaderComponent/Header.component';
import FooterComponent from './Components/FooterComponent/Footer.component';
import IntroduceComponent from './Components/ContentComponent/Introduce.component';

import SizeComponent from './Components/ContentComponent/Size.component';
import TypeComponent from './Components/ContentComponent/Type.component';
import DrinkComponent from './Components/ContentComponent/Drink.component';
import FormComponent from './Components/ContentComponent/Form.component';
function App() {
  return (
    <div className="App">
      <div className='container-fluid' style={{ background: "white" }}>
        <HeaderComponent />



        <div className='header container'>
          <h1 style={{ color: "orange" }}>Pizza 365</h1>
          <p style={{ color: "orange", fontStyle: "italic" }}>Truly italian!</p>

          <div className='slide-picture-menu'>




            {/* <img src={image} width={"100%"}></img> */}

            <Carousel>
              <Carousel.Item interval={1500}>
                <img
                  className="d-block w-100"
                  src={image1}
                  alt="Image One"
                />
              </Carousel.Item>
              <Carousel.Item interval={800}>
                <img
                  className="d-block w-100"
                  src={image2}
                  alt="Image Two"
                />
              </Carousel.Item>
              <Carousel.Item interval={800}>
                <img
                  className="d-block w-100"
                  src={image3}
                  alt="Image Two"
                />
              </Carousel.Item>
              <Carousel.Item interval={800}>
                <img
                  className="d-block w-100"
                  src={image4}
                  alt="Image Two"
                />
              </Carousel.Item>
              <Carousel.Item interval={800}>
                <img
                  className="d-block w-100"
                  src={image5}
                  alt="Image Two"
                />
              </Carousel.Item>
              <Carousel.Item interval={800}>
                <img
                  className="d-block w-100"
                  src={image6}
                  alt="Image Two"
                />
              </Carousel.Item>
              <Carousel.Item interval={800}>
                <img
                  className="d-block w-100"
                  src={image7}
                  alt="Image Two"
                />
              </Carousel.Item>
            </Carousel>



          </div>
        </div>
        <IntroduceComponent />
        <SizeComponent />
        <TypeComponent />

        <DrinkComponent />
        <FormComponent />

        {/* <!-- Footer --> */}
        <FooterComponent />



      </div>
    </div>
  );
}

export default App;
