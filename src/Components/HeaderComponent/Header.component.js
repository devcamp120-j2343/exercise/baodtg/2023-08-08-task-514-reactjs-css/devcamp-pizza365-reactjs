import { Component } from "react";
import { Navbar, Nav } from 'react-bootstrap';

class HeaderComponent extends Component {
    render() {
        return (
            <>

                <div className='div-menu container-fluid' style={{ marginBottom: "70px" }}>

                    <Navbar expand='lg' style={{ background: "orange", marginBottom: "50px" }}>
                        <Navbar.Brand href="#home" style={{ marginLeft: "50px", fontWeight: "700", fontSize: "30px" }}>PIZZA365</Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse className='pl-5' id="basic-navbar-nav">
                            <Nav className='mx-auto d-flex w-100 justify-content-evenly'>
                                <Nav.Link href="#home" >HOME</Nav.Link>
                                <Nav.Link href="#link">SIZE COMBO</Nav.Link>
                                <Nav.Link href="#link">PIZZA TYPES</Nav.Link>
                                <Nav.Link href="#link">FORM CONTACT</Nav.Link>
                            </Nav>

                        </Navbar.Collapse>
                    </Navbar>
                </div>

            </>
        )
    }
}
export default HeaderComponent