import { Component } from "react";

class FooterComponent extends Component {
    render() {
        return (
            <>
                <div className="container-fluid p-3" style={{ background: "orange" }}>
                    <div className="row text-center">
                        <div className="col-sm-12">
                            <h4 className="m-2">Footer</h4>


                            <a href="#" class="btn bg-main-brand text-white m-3" style={{ border: "1px solid lightgray", background: "lightgrey" }}><i class="fa fa-arrow-up mr-2"></i>To the top</a>
                            <div className="m-2">
                                <i className="fa fa-facebook-official text-main mx-1"></i>
                                <i className="fa fa-instagram text-main  mx-1"></i>
                                <i className="fa fa-snapchat text-main  mx-1"></i>
                                <i className="fa fa-pinterest-p text-main  mx-1"></i>
                                <i className="fa fa-twitter text-main  mx-1"></i>
                                <i className="fa fa-linkedin text-main  mx-1"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default FooterComponent