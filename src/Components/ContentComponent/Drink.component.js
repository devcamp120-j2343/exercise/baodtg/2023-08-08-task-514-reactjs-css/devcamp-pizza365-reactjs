import { Component } from "react";

class DrinkComponent extends Component {
    render() {
        return (
            <>
                <div className="div-drink-select text-center container  pt-3">
                    <h3 for="select-drink" style={{ color: "orange" }}>Hãy chọn đồ uống bạn yêu thích</h3>
                    <select className='w-100' style={{ height: "30px" }} name="drink-select" id="select-drink">
                        <option value="0">Chọn đồ uống</option>
                        <option value="PEPSI">Pepsi</option>
                        <option value="COCA">Cocacola</option>
                        <option value="TRASUA">Trà sữa</option>
                    </select>
                </div>
            </>
        )
    }
}
export default DrinkComponent