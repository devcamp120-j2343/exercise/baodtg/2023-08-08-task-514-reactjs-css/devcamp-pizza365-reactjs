import { Component } from "react";

class IntroduceComponent extends Component {
    render() {
        return (
            <>
                <div className='header-content'>
                    <h1 className='text-center mt-3' style={{ color: "orange" }}>Tại sao lại chọn Pizza365</h1>

                    <hr className='w-25 mx-auto' style={{ borderColor: "blue" }}></hr>

                    <div className='content'>
                        <div className='row pizza365-value'>
                            <div className='col-lg-3 value-1'>
                                <h4>Không ngừng cải tiến</h4>
                                <p style={{ fontWeight: "500" }}>Cải tiến sản phẩm, chất lượng và trải nghiệm khách hàng luôn là yếu tố tiên quyết trong chiến lược kinh doanh của chúng tôi nhằm thúc đẩy tăng trưởng và trở thành thương hiệu pizza hàng đầu.</p>
                            </div>
                            <div className='col-lg-3 value-2'>
                                <h4>Trải nghiệm dễ dàng</h4>
                                <p style={{ fontWeight: "500" }}>Chúng tôi đơn giản hướng đến việc trở thành thương hiệu của mọi nhà, mọi lúc, mọi nơi.</p>

                            </div>
                            <div className='col-lg-3 value-3'>
                                <h4>Hướng đến sự vượt trội</h4>
                                <p style={{ fontWeight: "500" }}>"Vượt trội" không chỉ là một khái niệm lớn. Thay vào đó, giá trị này là cốt lõi trong mỗi con người của chúng tôi, trong mỗi công việc mà họ làm.</p>


                            </div>
                            <div className='col-lg-3 value-4'>
                                <h4>Thể hiện đam mê</h4>
                                <p style={{ fontWeight: "500" }}>Chúng tôi sẵn lòng hỗ trợ vô điều kiện và linh động điều chỉnh tùy theo thử thách để tìm ra giải pháp.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default IntroduceComponent