import { Component } from "react";

class SizeComponent extends Component {
    render() {
        return (
            <>
                <div className='combo-choosing'>
                    <h1 className='text-center mt-3' style={{ color: "orange" }}>Menu combo Pizza 365</h1>
                    <hr className='w-25 mx-auto' style={{ borderColor: "blue" }}></hr>
                    <div className='combo-size-menu'>
                        <div className='row'>
                            <div className='col-lg-4'>
                                <div className="card">
                                    <div className="card-header bg-main-brand text-center" style={{ background: "orange" }}>
                                        <h3>S ( Small size)</h3>
                                    </div>
                                    <div className="card-body text-center">
                                        <ul className="list-group list-group-flush">
                                            <li className="list-group-item">
                                                Đường kính <b> 20 cm</b>
                                            </li>
                                            <li className="list-group-item">Sườn nướng <b>2</b></li>
                                            <li className="list-group-item">Salad <b>200 gr</b></li>
                                            <li className="list-group-item">Nước ngọt <b>2</b></li>
                                            <li className="list-group-item">
                                                <h1>VND <b>150,000</b></h1>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="card-footer text-center">
                                        <button className="btn w-100" style={{ background: "orange" }} id="btn-size-lgall" data-is-selected="N" >
                                            Chọn
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className='col-lg-4'>

                                <div className="card">
                                    <div className="card-header bg-main-brand text-center" style={{ background: "orange" }}>
                                        <h3>M (Medium size)</h3>
                                    </div>
                                    <div className="card-body text-center">
                                        <ul className="list-group list-group-flush">
                                            <li className="list-group-item">
                                                Đường kính <b> 25 cm</b>
                                            </li>
                                            <li className="list-group-item">Sườn nướng <b>4</b></li>
                                            <li className="list-group-item">Salad <b>300 gr</b></li>
                                            <li className="list-group-item">Nước ngọt <b>3</b></li>
                                            <li className="list-group-item">
                                                <h1>VND <b>200,000</b></h1>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="card-footer text-center">
                                        <button className="btn w-100" style={{ background: "orange" }} id="btn-size-medium" data-is-selected="N" >
                                            Chọn
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div className='col-lg-4'>

                                <div className="card">
                                    <div className="card-header bg-main-brand text-center" style={{ background: "orange" }}>
                                        <h3>L ( Large size )</h3>
                                    </div>
                                    <div className="card-body text-center">
                                        <ul className="list-group list-group-flush">
                                            <li className="list-group-item">
                                                Đường kính <b> 30 cm</b>
                                            </li>
                                            <li className="list-group-item">Sườn nướng <b>8</b></li>
                                            <li className="list-group-item">Salad <b>500 gr</b></li>
                                            <li className="list-group-item">Nước ngọt <b>4</b></li>
                                            <li className="list-group-item">
                                                <h1>VND <b>250,000</b></h1>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="card-footer text-center">
                                        <button className="btn w-100" style={{ background: "orange" }} id="btn-size-large" data-is-selected="N" >
                                            Chọn
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default SizeComponent