import { Component } from "react";

class FormComponent  extends Component {
    render() {
        return (
            <>
                <div className='container text-center pt-3'>
                    <h3 for="contact" style={{ color: "orange" }}>Thông tin đơn hàng</h3>
                    {/* <!-- Content Form Contact Us --> */}
                    <div className="col-lg-12 p-2 jumbotron text-start">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="form-group mt-3">
                                    <label for="input-name">Họ và tên</label>
                                    <input type="text" className="form-control" id="input-name" placeholder="Họ và tên" />
                                </div>
                                <div className="form-group mt-3">
                                    <label for="email">Email</label>
                                    <input type="text" className="form-control" id="input-email" placeholder="Email" />
                                </div>
                                <div className="form-group mt-3">
                                    <label for="input-phone">Điện thoại</label>
                                    <input type="text" className="form-control" id="input-phone" placeholder="Điện thoại" />
                                </div>
                                <div className="form-group mt-3">
                                    <label for="input-address">Địa chỉ</label>
                                    <input type="text" className="form-control" id="input-address" placeholder="Địa chỉ" />
                                </div>
                                <div className="form-group mt-3">
                                    <label for="message">Lời nhắn</label>
                                    <input type="text" className="form-control" id="input-message" placeholder="Lời nhắn" />
                                </div>
                                <div className="form-group mt-3">
                                    <label for="inp-voucher">Mã giảm giá ( Voucher ID) </label>
                                    <input type="text" className="form-control" id="input-voucher" placeholder="Mã voucher..." />
                                </div>
                                <button type="button" className="btn w-100 mt-3" style={{ background: "orange" }}> Kiểm tra đơn </button>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default FormComponent