import { Component } from "react";
import pizzaHaiSan from '../../assets/images/pizza_type_hai_san.jpg'
import pizzaHawai from '../../assets/images/pizza_type_hawai.jpg'
import pizzaThitNuong from '../../assets/images/pizza_type_thit_nuong.jpg'
class TypeComponent extends Component {
    render() {
        return (
            <>
                <div className='pizza-choosing'>
                    <h1 className='text-center mt-3' style={{ color: "orange" }}>Chọn loại pizza</h1>
                    <hr className='w-25 mx-auto' style={{ borderColor: "blue" }}></hr>
                    <div className='pizza-menu'>
                        <div className='row '>
                            <div className='col-lg-4'>
                                <div className="card">
                                    <img className="card-img-top" src={pizzaHaiSan} alt="Card image cap"></img>
                                    <div className="card-body">
                                        <h5 className="card-title">Pizza Hải Sản</h5>
                                        <p className="card-text">Món ăn thanh đạm </p>

                                        <p className="card-text">Hãy thưởng thức món ăn với phong cách Alo Ha đến từ Hawai.</p>
                                        <a href="#" class="btn w-100" style={{ background: "orange" }}>Chọn</a>
                                    </div>
                                </div>
                            </div>
                            <div className='col-lg-4'>
                                <div className="card">
                                    <img className="card-img-top" src={pizzaHawai} alt="Card image cap"></img>
                                    <div className="card-body">
                                        <h5 className="card-title">Pizza Hawai</h5>
                                        <p className="card-text">Món ăn thanh đạm </p>

                                        <p className="card-text">Hãy thưởng thức món ăn với phong cách Alo Ha đến từ Hawai.</p>
                                        <a href="#" class="btn  w-100" style={{ background: "orange" }}>Chọn</a>
                                    </div>
                                </div>
                            </div>
                            <div className='col-lg-4'>
                                <div className="card">
                                    <img className="card-img-top" src={pizzaThitNuong} alt="Card image cap"></img>
                                    <div className="card-body">
                                        <h5 className="card-title">Pizza Thịt Nướng</h5>
                                        <p className="card-text">Món ăn thanh đạm </p>
                                        <p className="card-text">Hãy thưởng thức món ăn với phong cách Alo Ha đến từ Hawai.</p>
                                        <a href="#" class="btn w-100" style={{ background: "orange" }}>Chọn</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default TypeComponent